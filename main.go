package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

func main() {
	from := flag.String("from", "https://updates.jenkins.io/download/",
		"string to replace (old)")
	into := flag.String("into", "https://archives.jenkins-ci.org/",
		"string to replace with (new)")
	url := flag.String("url", "https://updates.jenkins.io/update-center.json",
		"update center base URL")
	version := flag.String("version", "2.263.1", "Jenkins version")
	flag.Parse()

	buf, err := fetch(*url, *version)
	if err != nil {
		log.Fatal(err)
	}

	// cloudbee people call it JSON, but it's not: it starts with
	// "updateCenter.post(", new line, only then lots of JSON, new line,
	// and it ends in ");"
	s := strings.ReplaceAll(string(buf), *from, *into)
	fmt.Fprintf(os.Stdout, s)
}

func fetch(baseUrl string, version string) ([]byte, error) {
	const (
		paramID      = "id"
		paramVersion = "version"
		defaultID    = "default"
	)
	u, err := url.Parse(baseUrl)
	if err != nil {
		return nil, err
	}

	// optionally add query parameters 'id' and 'version'
	anything := func(s string) bool {
		return len(s) > 0
	}
	q := u.Query()
	hasValue := func(key string) bool {
		return anything(q.Get(key))
	}
	hasId := hasValue(paramID)
	hasVersion := hasValue(version)
	complete := hasId && hasVersion
	if !complete {
		if !hasId {
			q.Set(paramID, defaultID)
		}
		if !hasVersion {
			q.Set(paramVersion, version)
		}
		u.RawQuery = q.Encode()
	}
	log.Printf("requesting %q\n", u)
	res, err := http.Get(u.String())
	if err != nil {
		return nil, fmt.Errorf("error requesting %q: %+w", u, err)
	}
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("want HTTP status code %d but got %d", http.StatusOK, res.StatusCode)
	}

	r := res.Body
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// read response body
	buf, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	return buf, nil
}
